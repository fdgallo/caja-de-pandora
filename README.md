# Caja de pandora

La Caja de Pandora es un repositorio que contiene binarios de malware recopilado desde el año 2005 por Facundo Gallo.

El único objetivo de la Caja de Pandora es poner en común un listado de malware actualizado para los investigadores en ciberseguridad y cibercrímen, así como también aportar ficheros originales que puedan ser tratados con fines formativos.

Es importante remarcar que la colección contiene únicamente ficheros compilados y/o samples de código fuente. Los sistemas completos en caso de RATs (cliente-servidor), Botnets (CNC), Ransomwares u otras herramientas (Shell inversa, joiners, etc.) se encuetran en un repositorio privado. En caso de necesitar acceder a este tipo de contenido, puede contactar con el administrador de la Caja de Pandora.

Con todo ello, se desaconseja y reprueba el uso del malware para fines ilícitos.


## Aviso de seguridad

Por favor, recuerda que el malware contenido en la Caja de Pandora puede suponer la pérdida, cifrado o sustracción de datos, es por este motivo que todos los ejemplares se encuentran comprimidos con contraseña, siendo la misma "infected".

No olvides ejecutarlos siempre en un entorno controlado.
